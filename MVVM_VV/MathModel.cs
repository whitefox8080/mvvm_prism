﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Mvvm;

namespace MVVM_VV
{
    public class MathModel: BindableBase
    {
        private readonly ObservableCollection<int> _values = new ObservableCollection<int>();
        public readonly ReadOnlyObservableCollection<int> Values;

        public MathModel()
        {
            Values = new ReadOnlyObservableCollection<int>(_values);
        }

        public void AddValue(int value)
        {
            _values.Add(value);
            RaisePropertyChanged("Sum");
        }

        public void RemoveValue(int index)
        {
            if(index >= 0 && index < _values.Count) _values.RemoveAt(index);
            RaisePropertyChanged("Sum");
        }

        public int Sum => Values.Sum();
    }
}
