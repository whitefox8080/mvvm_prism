﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Commands;
using Prism.Mvvm;

namespace MVVM_VV
{
    public class MainVM: BindableBase
    {
        readonly MathModel _model = new MathModel();

        public MainVM()
        {
            // from model to view
            _model.PropertyChanged += (s, e) => { RaisePropertyChanged(e.PropertyName); };

            AddCommand = new DelegateCommand<string>(str =>
            {
                int ival;
                if(int.TryParse(str, out ival)) _model.AddValue(ival);
            });
            RemoveCommand = new DelegateCommand<int?>(i =>
            {
                if(i.HasValue) _model.RemoveValue(i.Value);
            });
        }

        public DelegateCommand<string> AddCommand { get; }
        public DelegateCommand<int?> RemoveCommand { get; }

        public int Sum => _model.Sum;
        public ReadOnlyObservableCollection<int> Values => _model.Values;
    }
}
